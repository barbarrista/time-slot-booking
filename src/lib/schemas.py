from collections.abc import Iterable
from typing import Any, Self

from pydantic import BaseModel, ConfigDict


def snake_to_camel(name: str) -> str:
    first, *rest = name.split("_")
    return first + "".join(map(str.capitalize, rest))


class BaseSchema(BaseModel):
    model_config = ConfigDict(
        from_attributes=True,
        populate_by_name=True,
        alias_generator=snake_to_camel,
    )

    @classmethod
    def model_validate_optional(cls, model: object | None) -> Self | None:
        if model is None:
            return None
        return cls.model_validate(model)

    @classmethod
    def model_validate_list(cls, models: Iterable[Any]) -> list[Self]:
        return [cls.model_validate(model) for model in models]
