import logging

import sentry_sdk

from lib.settings import get_settings

from .settings import AppSettings, SentrySettings


def setup_telemetry() -> None:
    settings = get_settings(AppSettings)

    logging.basicConfig(
        level=settings.logging_level.name,
        format="[%(asctime)s] %(message)s",
    )
    _init_sentry()


def _init_sentry() -> None:
    settings = get_settings(SentrySettings)
    sentry_sdk.init(
        dsn=settings.dsn,
        environment=settings.environment,
        traces_sample_rate=settings.traces_sample_rate,
    )
