import contextlib
from collections.abc import AsyncIterator, Sequence

from aiogram import Bot, Dispatcher, Router
from aiogram.client.session.aiohttp import AiohttpSession
from aiogram.types import BotCommand
from aioinject import Container

from app import telemetry
from app.di import create_container
from app.settings import TelegramSettings
from lib.settings import get_settings

from .middleware import (
    EnsureAccountMiddleware,
    InjectMiddleware,
    SentryMiddleware,
)
from .routers import (
    account,
    booking,
    registration,
    start,
)
from .routers import (
    settings as user_settings,
)

ROUTERS: Sequence[Router] = [
    registration.router,
    booking.router,
    start.router,
    user_settings.router,
    account.router,
]


@contextlib.asynccontextmanager
async def create_bot() -> AsyncIterator[Bot]:
    settings = get_settings(TelegramSettings)
    async with AiohttpSession() as session:
        yield Bot(token=settings.token, session=session)


def _configure_middlewares(container: Container, main_router: Router) -> None:
    main_router.message.outer_middleware.register(InjectMiddleware(container))
    main_router.callback_query.outer_middleware.register(InjectMiddleware(container))

    main_router.message.outer_middleware.register(SentryMiddleware())
    main_router.message.outer_middleware.register(EnsureAccountMiddleware())


def _register_routes(main_router: Router) -> None:
    for router in ROUTERS:
        main_router.include_router(router)


async def main() -> None:
    telemetry.setup_telemetry()

    container = create_container()
    main_router = Router()

    _configure_middlewares(container, main_router)
    _register_routes(main_router)

    dispatcher = Dispatcher()
    dispatcher.include_router(main_router)

    async with create_bot() as bot:
        await bot.set_my_commands(
            commands=[
                BotCommand(command="start", description="Старт"),
                BotCommand(command="settings", description="Настройки"),
                BotCommand(command="account", description="Аккаунт"),
            ],
        )
        await dispatcher.start_polling(bot)
