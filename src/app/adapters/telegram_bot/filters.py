from typing import Annotated, Any, Final

from aiogram.filters import Filter
from aiogram.filters.callback_data import CallbackQueryFilter
from aiogram.types import Message
from aioinject import Inject, inject

from app.core.telegram_user.repository import TelegramUserRepository


class AccountFilter(Filter):
    user_key: Final = "user"

    @inject
    async def __call__(
        self,
        message: Message,
        repository: Annotated[TelegramUserRepository, Inject],
        **kwargs: Any,  # noqa:  ANN401, ARG002
    ) -> dict[str, Any] | bool:
        if (request_user := message.from_user) is None:
            return False

        user = await repository.get(id_=request_user.id)
        if user is None:
            return False

        return {self.user_key: user}


class SendUserPhoneNumberFilter(CallbackQueryFilter):
    pass
