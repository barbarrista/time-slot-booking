from jinja2 import Environment, PackageLoader

env = Environment(
    loader=PackageLoader("app.adapters.telegram_bot"),
    autoescape=True,
)
