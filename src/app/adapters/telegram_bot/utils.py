from aiogram import Bot
from aiogram.types import CallbackQuery, MaybeInaccessibleMessage, Message

from lib.null_safety import getval


async def remove_keyboard_(callback: CallbackQuery) -> None:
    bot = getval(callback.bot)
    message = getval(callback.message)
    message_id = message.message_id if isinstance(message, Message) else None
    await bot.edit_message_reply_markup(
        chat_id=callback.from_user.id,
        message_id=message_id,
        reply_markup=None,
    )


async def remove_keyboard(bot: Bot, *, message: MaybeInaccessibleMessage) -> None:
    if not isinstance(message, Message):
        return

    await bot.edit_message_reply_markup(
        chat_id=getval(message.from_user).id,
        message_id=message.message_id,
        reply_markup=None,
    )
