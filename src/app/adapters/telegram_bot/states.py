from aiogram.fsm.state import State, StatesGroup


class RegistrationStateGroup(StatesGroup):
    WAITING_PHONE_NUMBER = State()
