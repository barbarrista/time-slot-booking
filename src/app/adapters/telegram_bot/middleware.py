from collections.abc import Awaitable, Callable
from typing import Annotated, Any

import sentry_sdk
from aiogram import BaseMiddleware
from aiogram.types import InlineKeyboardButton, Message, TelegramObject, User
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aioinject import Container, Inject, inject

from app.adapters.telegram_bot.callbacks import RegistrationCallback
from app.core.telegram_user.dto import TelegramUserCreateDTO
from app.core.telegram_user.repository import TelegramUserRepository


class InjectMiddleware(BaseMiddleware):
    def __init__(self, container: Container) -> None:
        self._container = container

    async def __call__(
        self,
        handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
        event: TelegramObject,
        data: dict[str, Any],
    ) -> Any:  # noqa: ANN401
        async with self._container.context():
            return await handler(event, data)


class SentryMiddleware(BaseMiddleware):
    async def __call__(
        self,
        handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
        event: TelegramObject,
        data: dict[str, Any],
    ) -> Any:  # noqa: ANN401
        try:
            return await handler(event, data)
        except Exception as e:
            sentry_sdk.capture_exception(e)
            raise


class EnsureAccountMiddleware(BaseMiddleware):
    @inject
    async def __call__(  # type:ignore[override]  # for working DI injection
        self,
        handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
        event: TelegramObject,
        data: dict[str, Any],
        repository: Annotated[TelegramUserRepository, Inject],
    ) -> Any:  # noqa: ANN401
        handle = handler(event, data)
        if not isinstance(event, Message) or not event.from_user:
            return await handle

        tg_user = event.from_user

        if event.from_user.is_bot:
            await event.answer("Бот не может обращаться ко мне!")
            return None

        await self._process_tg_user(tg_user, repository=repository)
        return await handle

    async def _process_tg_user(
        self,
        tg_user: User,
        *,
        repository: TelegramUserRepository,
    ) -> None:
        if await repository.get(id_=tg_user.id):
            return

        await repository.create(
            dto=TelegramUserCreateDTO(
                id=tg_user.id,
                first_name=tg_user.first_name,
                last_name=tg_user.last_name,
            ),
        )


class RegisteredUserRequiredMiddleware(BaseMiddleware):
    @inject
    async def __call__(  # type:ignore[override]  # for working DI injection
        self,
        handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]],
        event: TelegramObject,
        data: dict[str, Any],
        repository: Annotated[TelegramUserRepository, Inject],
    ) -> Any:  # noqa: ANN401
        handle = handler(event, data)
        if not isinstance(event, Message) or not event.from_user:
            return await handle

        tg_user = event.from_user
        user = await repository.get(id_=tg_user.id)
        if user is None:
            return await handle

        if user.phone_number is None:
            builder = InlineKeyboardBuilder()
            builder.add(
                InlineKeyboardButton(
                    text="Зарегистрироваться",
                    callback_data=RegistrationCallback().pack(),
                ),
            )

            return await event.answer(
                "Для начала, зарегистрируйтесь",
                reply_markup=builder.as_markup(one_time_keyboard=True),
            )
        return await handle
