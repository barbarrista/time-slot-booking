from typing import Annotated

from aiogram import Router
from aiogram.filters import Command
from aiogram.handlers import CallbackQueryHandler
from aiogram.types import (
    InlineKeyboardButton,
    Message,
)
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aioinject import Inject, inject
from result import Err

from app.adapters.telegram_bot.callbacks import (
    CancelReservationCallback,
    MayBeCancelReservationCallback,
    PaginateReservedSlotsCallback,
    ShowReservedSlotsCallback,
)
from app.adapters.telegram_bot.filters import AccountFilter
from app.adapters.telegram_bot.keyboards import account
from app.adapters.telegram_bot.keyboards.row import KeyboardRow
from app.adapters.telegram_bot.middleware import RegisteredUserRequiredMiddleware
from app.adapters.telegram_bot.mixins import HasAccountMixin, RemoveKeyboardMixin
from app.core.booking.commands.cancel_reservation import CancelSlotReservationCommand
from app.core.booking.dto import MayBeCancelSlotReservationDTO, ReservedSlotFilterDTO
from app.core.booking.queries.reserved_slots import ReservedSlotsQuery
from app.core.booking.repository import BookingRepository
from app.core.dto import PaginationResultDTO
from app.core.markdown.service import MarkdownService
from app.db.models.booking import ReservedSlot
from app.db.models.telegram_user import TelegramUser
from lib.di import INJECTED
from lib.null_safety import getval
from lib.utils import pack, utc_now

router = Router()
router.message.outer_middleware(RegisteredUserRequiredMiddleware())
router.message.middleware(RegisteredUserRequiredMiddleware())


@router.message(Command(commands=["account"]))
async def account_handler(message: Message) -> None:
    await message.answer(
        text="Выберите действие",
        reply_markup=account.BUILDER.as_markup(),
    )


@router.callback_query(PaginateReservedSlotsCallback.filter(), AccountFilter())
class PaginateReservedSlotsHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(
        self,
        query: Annotated[ReservedSlotsQuery, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()
        callback_data = PaginateReservedSlotsCallback.unpack(getval(self.callback_data))
        result = await query.execute(self.db_user, page=callback_data.page)

        if not result.items:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="У Вас отсутствуют забронированные слоты",  # noqa: RUF001
            )
            return

        builder = self._get_keyboard_builder(callback_data.page, result=result)
        await self._edit_message_reply_markup(builder)

    async def _edit_message_reply_markup(self, builder: InlineKeyboardBuilder) -> None:
        message = getval(self.message)
        if not isinstance(message, Message):
            return

        await self.bot.edit_message_reply_markup(
            chat_id=self.from_user.id,
            message_id=message.message_id,
            reply_markup=builder.as_markup(resize=True),
        )

    def _get_keyboard_builder(
        self,
        page: int,
        result: PaginationResultDTO[ReservedSlot],
    ) -> InlineKeyboardBuilder:
        buttons = (
            InlineKeyboardButton(
                text=_get_text(user=self.db_user, reserved_slot=reserved_slot),
                callback_data=MayBeCancelReservationCallback(
                    reserved_slot_id=reserved_slot.id,
                ).pack(),
            )
            for reserved_slot in result.items
        )

        row = KeyboardRow()
        packed_buttons = pack(buttons, size=1)
        if page != 1:
            row.append(
                account.get_paginate_button(page - 1, direction="prev"),
            )
        if result.has_next_page:
            row.append(
                account.get_paginate_button(page + 1, direction="next"),
            )

        packed_buttons.extend([row.list])
        return InlineKeyboardBuilder(packed_buttons)


@router.callback_query(ShowReservedSlotsCallback.filter(), AccountFilter())
class ShowBookingSlotsHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(
        self,
        query: Annotated[ReservedSlotsQuery, Inject] = INJECTED,
        md_service: Annotated[MarkdownService, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()

        page = 1
        result = await query.execute(self.db_user, page=page)

        if not result.items:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="У Вас отсутствуют забронированные слоты",  # noqa: RUF001
            )
            return

        builder = self._get_keyboard_builder(page, result)
        await self.bot.send_message(
            chat_id=self.from_user.id,
            text=md_service.show_booking_slots_info(tz_name=self.db_user.tz_name),
            parse_mode="Markdown",
            reply_markup=builder.as_markup(resize=True),
        )

    def _get_keyboard_builder(
        self,
        page: int,
        result: PaginationResultDTO[ReservedSlot],
    ) -> InlineKeyboardBuilder:
        buttons = (
            InlineKeyboardButton(
                text=_get_text(user=self.db_user, reserved_slot=reserved_slot),
                callback_data=MayBeCancelReservationCallback(
                    reserved_slot_id=reserved_slot.id,
                ).pack(),
            )
            for reserved_slot in result.items
        )

        packed_buttons = pack(buttons, size=1)
        if result.has_next_page:
            packed_buttons.extend(
                [[account.get_paginate_button(page + 1, direction="next")]],
            )

        return InlineKeyboardBuilder(packed_buttons)


def _get_text(user: TelegramUser, reserved_slot: ReservedSlot) -> str:
    represent_datetime = (
        f"{reserved_slot.booking_slot.represent_datetime(user.timezone)}"
    )
    icon = "☑️" if reserved_slot.is_approved else "⏳"
    return f"{represent_datetime}\n{icon}"


@router.callback_query(MayBeCancelReservationCallback.filter(), AccountFilter())
class MayBeCancelReservationHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(
        self,
        repository: Annotated[BookingRepository, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()

        reserved_slot = await self._get_slot(repository)
        if reserved_slot is None:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Выбранный слот отсутствует",
            )
            return

        builder = self._get_keyboard_builder(reserved_slot)
        date_info = reserved_slot.booking_slot.represent_datetime(self.db_user.timezone)
        text = f'Выбран слот "{date_info}"\nПодтверждаете отмену бронирования?'  # noqa: RUF001

        await self.bot.send_message(
            chat_id=self.from_user.id,
            text=text,
            reply_markup=builder.as_markup(one_time_keyboard=True, resize=True),
        )

    async def _get_slot(self, repository: BookingRepository) -> ReservedSlot | None:
        callback_data = MayBeCancelReservationCallback.unpack(
            getval(self.callback_data),
        )
        return await repository.get_reserved_slot(
            dto=ReservedSlotFilterDTO(id=callback_data.reserved_slot_id),
            time_offset=self.db_user.tz_delta,
            start_time_gte=utc_now(),
        )

    def _get_keyboard_builder(self, slot: ReservedSlot) -> InlineKeyboardBuilder:
        buttons = [
            InlineKeyboardButton(
                text=text,
                callback_data=CancelReservationCallback(
                    reserved_slot_id=slot.id,
                    is_confirmed=is_confirmed,
                ).pack(),
            )
            for text, is_confirmed in (("Да", True), ("Нет", False))
        ]
        return InlineKeyboardBuilder(markup=[buttons])


@router.callback_query(CancelReservationCallback.filter(), AccountFilter())
class CancelReservationHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(
        self,
        command: Annotated[CancelSlotReservationCommand, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()
        callback_data = CancelReservationCallback.unpack(getval(self.callback_data))

        if not callback_data.is_confirmed:
            await self.event.answer(text="Действие отменено")
            return

        result = await command.execute(
            dto=MayBeCancelSlotReservationDTO(
                reserved_slot_id=callback_data.reserved_slot_id,
                user=self.db_user,
            ),
        )

        if isinstance(result, Err):
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Слот отсутствует или бронь уже была ранее отменена",
            )
            return

        await self.bot.send_message(
            chat_id=self.from_user.id,
            text="Бронирование слота было отменено",
        )
