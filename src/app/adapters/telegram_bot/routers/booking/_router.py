from collections.abc import Sequence
from datetime import date
from typing import Annotated
from uuid import UUID

from aiogram import Router
from aiogram.handlers import CallbackQueryHandler
from aiogram.types import (
    InlineKeyboardButton,
    ReplyKeyboardRemove,
)
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aioinject import Inject, inject
from result import Err

from app.adapters.telegram_bot.callbacks import (
    PickDateCallback,
    PickSlotCallback,
    PickTimeCallback,
    StartBookingCallback,
)
from app.adapters.telegram_bot.filters import AccountFilter
from app.adapters.telegram_bot.mixins import HasAccountMixin, RemoveKeyboardMixin
from app.core.booking import BookingRepository
from app.core.booking.commands.reserve_slot import ReserveSlotCommand
from app.core.booking.dto import FreeDateSlotsDTO, MayBeReserveSlotDTO
from app.core.booking.queries.free_date_slots import FreeDateSlotsQuery
from app.db.models.booking import BookingSlot
from lib.di import INJECTED
from lib.null_safety import getval
from lib.utils import pack, represent_date, utc_now

router = Router()
router.message.filter(AccountFilter())
router.callback_query.filter(AccountFilter())


@router.callback_query(StartBookingCallback.filter(), AccountFilter())
class StartBookingHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(  # type: ignore[override]
        self,  # ↑ DI moment ↑
        query: Annotated[FreeDateSlotsQuery, Inject],
    ) -> None:
        await self._remove_keyboard()

        master_id = UUID(
            "58947aac-441d-4091-896a-78574c57e93e",
        )  # TODO(@barbarrista): remove hardcode # noqa: TD003, FIX002

        result = await query.execute(
            dto=FreeDateSlotsDTO(
                user=self.db_user,
                master_id=master_id,
                start_time_gte=utc_now(),
            ),
        )
        if isinstance(result, Err):
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Произошла ошибка, специалист не найден",
                reply_markup=ReplyKeyboardRemove(),
            )
            return

        if not (dates := result.ok_value):
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Сожалеем, но на текущий момент свободных записей нет",
                reply_markup=ReplyKeyboardRemove(),
            )
            return

        builder = self._get_keyboard_builder(dates)

        await self.bot.send_message(
            chat_id=self.from_user.id,
            text="Выберите удобную для Вас дату",  # noqa: RUF001
            reply_markup=builder.as_markup(one_time_keyboard=True, resize=True),
        )

    def _get_keyboard_builder(self, dates: Sequence[date]) -> InlineKeyboardBuilder:
        buttons = pack(
            (
                InlineKeyboardButton(
                    text=represent_date(date),
                    callback_data=PickDateCallback(date=date).pack(),
                )
                for date in dates
            ),
            size=3,
        )
        return InlineKeyboardBuilder(buttons)


@router.callback_query(PickDateCallback.filter(), AccountFilter())
class PickDateHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    @inject
    async def handle(
        self,
        repository: Annotated[BookingRepository, Inject] = INJECTED,  # DI moment
    ) -> None:
        await self._remove_keyboard()
        callback_data = PickDateCallback.unpack(getval(self.callback_data))
        slots = await repository.get_free_slots(
            booking_date=callback_data.date,
            start_time_gte=utc_now() + self.db_user.tz_delta,
        )
        if not slots:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Сожалеем, но на текущую дату свободные слоты отсутствуют",
                reply_markup=ReplyKeyboardRemove(),
            )
            return

        builder = self._get_keyboard_builder(slots)
        await self.bot.send_message(
            chat_id=self.from_user.id,
            text=f"Выберите удобное для Вас время ({self.db_user.tz_name})",  # noqa: RUF001
            reply_markup=builder.as_markup(one_time_keyboard=True),
        )

    def _get_keyboard_builder(
        self,
        slots: Sequence[BookingSlot],
    ) -> InlineKeyboardBuilder:
        buttons = pack(
            (
                InlineKeyboardButton(
                    text=slot.represent_time_period(delta=self.db_user.tz_delta),
                    callback_data=PickTimeCallback(slot_id=slot.id).pack(),
                )
                for slot in slots
            ),
            size=3,
        )
        return InlineKeyboardBuilder(buttons)


@router.callback_query(PickTimeCallback.filter(), AccountFilter())
class PickTimeHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):
    @inject
    async def handle(
        self,
        repository: Annotated[BookingRepository, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()
        callback_data = PickTimeCallback.unpack(getval(self.callback_data))
        slot = await repository.get_booking_slot(ident=callback_data.slot_id)
        if slot is None:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Выбранный слот отсутствует или уже занят",
                reply_markup=ReplyKeyboardRemove(),
            )
            return

        builder = self._get_keyboard_builder(slot)
        await self.bot.send_message(
            chat_id=self.from_user.id,
            text=f'Выбран слот "{slot.represent_time_period(delta=self.db_user.tz_delta)}\n'
            "Подтверждаете бронирование?",
            reply_markup=builder.as_markup(one_time_keyboard=True, resize=True),
        )

    def _get_keyboard_builder(self, slot: BookingSlot) -> InlineKeyboardBuilder:
        buttons = [
            InlineKeyboardButton(
                text=text,
                callback_data=PickSlotCallback(
                    slot_id=slot.id,
                    is_booking_approved=is_approved,
                ).pack(),
            )
            for text, is_approved in (("Да", True), ("Нет", False))
        ]
        return InlineKeyboardBuilder(markup=[buttons])


@router.callback_query(PickSlotCallback.filter(), AccountFilter())
class ReserveSlotHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):
    @inject
    async def handle(
        self,
        command: Annotated[ReserveSlotCommand, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()

        callback_data = PickSlotCallback.unpack(getval(self.callback_data))
        if callback_data.is_booking_approved is False:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Бронирование слота отменено",
            )
            return

        dto = MayBeReserveSlotDTO(slot_id=callback_data.slot_id, user=self.db_user)
        result = await command.execute(dto)
        if isinstance(result, Err):
            await self.bot.send_message(
                chat_id=self.from_user.id,
                text="Выбранный слот отсутствует или уже занят",
            )
            return

        await self.bot.send_message(
            chat_id=self.from_user.id,
            text="Слот забронирован. Ожидайте подтверждения бронирования от администратора",
        )
