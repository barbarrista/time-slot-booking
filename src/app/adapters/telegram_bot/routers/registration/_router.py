from typing import Annotated

from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.handlers import CallbackQueryHandler, MessageHandler
from aiogram.types import (
    ContentType,
    Message,
    ReplyKeyboardRemove,
)
from aioinject import Inject, inject

from app.adapters.telegram_bot.callbacks import (
    RegistrationCallback,
)
from app.adapters.telegram_bot.filters import AccountFilter
from app.adapters.telegram_bot.keyboards import send_phone_number, start_booking
from app.adapters.telegram_bot.mixins import HasAccountMixin, RemoveKeyboardMixin
from app.adapters.telegram_bot.states import RegistrationStateGroup
from app.core.telegram_user.dto import TelegramUserUpdateDTO
from app.core.telegram_user.repository import TelegramUserRepository
from lib.di import INJECTED
from lib.null_safety import getval

router = Router()
router.message.filter()


@router.callback_query(AccountFilter(), RegistrationCallback.filter())
class StartRegistrationHandler(
    CallbackQueryHandler,
    HasAccountMixin,
    RemoveKeyboardMixin,
):

    async def handle(self) -> None:
        state: FSMContext = self.data["state"]
        await self._remove_keyboard()
        message = getval(self.message)
        message_id = message.message_id if isinstance(message, Message) else None

        if self.db_user.has_phone_number:
            await self.bot.send_message(
                chat_id=self.from_user.id,
                reply_to_message_id=message_id,
                text="Вы уже были ранее зарегистрированы!",
                reply_markup=ReplyKeyboardRemove(),
            )
            return

        await self.bot.send_message(
            chat_id=self.from_user.id,
            text="Для регистрации отправьте свой номер телефона",
            reply_markup=send_phone_number.BUILDER.as_markup(
                one_time_keyboard=True,
                resize=True,
            ),
        )
        await state.set_state(RegistrationStateGroup.WAITING_PHONE_NUMBER)


@router.message(
    AccountFilter(),
    RegistrationStateGroup.WAITING_PHONE_NUMBER,
    F.content_type == ContentType.CONTACT,
)
class ReceivePhoneNumberHandler(MessageHandler, HasAccountMixin):

    @inject
    async def handle(
        self,
        repository: Annotated[TelegramUserRepository, Inject] = INJECTED,
    ) -> None:
        state: FSMContext = self.data["state"]

        if (contact := self.event.contact) is None:
            await self.event.answer(text="Номер телефона не был отправлен!")
            return

        from_user = getval(self.from_user)
        if contact.user_id != from_user.id:
            await self.event.answer(text="Отправьте собственный номер телефона!")
            return

        await repository.update(
            user=self.db_user,
            dto=TelegramUserUpdateDTO(
                first_name=from_user.first_name,
                last_name=from_user.last_name,
                phone_number=contact.phone_number,
                is_verified=False,
            ),
        )
        await self.event.answer(
            text=f'Регистрация пройдена успешно. Установлен часовой пояс "{self.db_user.tz_name}"\n'
            "Для его смены отправьте команду /settings",  # noqa: RUF001
            reply_markup=ReplyKeyboardRemove(),
        )
        await state.clear()

        await self.bot.send_message(
            chat_id=from_user.id,
            text="Теперь, Вы можете записаться на консультацию",
            reply_markup=start_booking.BUILDER.as_markup(
                one_time_keyboard=True,
                resize=True,
            ),
        )
