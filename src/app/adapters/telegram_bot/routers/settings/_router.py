from typing import Annotated

from aiogram import Router
from aiogram.filters import Command
from aiogram.handlers import CallbackQueryHandler
from aiogram.types import (
    Message,
    ReplyKeyboardRemove,
)
from aiogram.utils.keyboard import (
    InlineKeyboardBuilder,
)
from aioinject import Inject, inject

from app.adapters.telegram_bot.callbacks import (
    EditTimezoneCallback,
    TimezoneCallback,
)
from app.adapters.telegram_bot.filters import AccountFilter
from app.adapters.telegram_bot.keyboards import change_settings
from app.adapters.telegram_bot.middleware import RegisteredUserRequiredMiddleware
from app.adapters.telegram_bot.mixins import HasAccountMixin, RemoveKeyboardMixin
from app.core.telegram_user.repository import TelegramUserRepository
from app.core.timezones import TimeZone
from lib.di import INJECTED
from lib.null_safety import getval

router = Router()
router.message.outer_middleware(RegisteredUserRequiredMiddleware())
router.message.middleware(RegisteredUserRequiredMiddleware())


@router.message(Command(commands=["settings"]))
async def settings_handler(message: Message) -> None:
    await message.answer(
        text="Выберите действие",
        reply_markup=change_settings.BUILDER.as_markup(one_time_keyboard=True),
    )


@router.callback_query(EditTimezoneCallback.filter(), AccountFilter())
class EditTimezoneHandler(CallbackQueryHandler, HasAccountMixin, RemoveKeyboardMixin):
    async def handle(self) -> None:
        await self._remove_keyboard()
        builder = InlineKeyboardBuilder(change_settings.get_timezone_buttons())
        await self.bot.send_message(
            chat_id=self.from_user.id,
            text=f'Текущий часовой пояс: "{self.db_user.tz_name}"\nВыберите часовой пояс',  # noqa: RUF001
            reply_markup=builder.as_markup(resize=True),
        )


@router.callback_query(TimezoneCallback.filter(), AccountFilter())
class SetTimezoneHandler(CallbackQueryHandler, HasAccountMixin, RemoveKeyboardMixin):

    @inject
    async def handle(
        self,
        repository: Annotated[TelegramUserRepository, Inject] = INJECTED,
    ) -> None:
        await self._remove_keyboard()

        callback_data = TimezoneCallback.unpack(getval(self.callback_data))
        time_zone = TimeZone[callback_data.time_zone]
        await repository.set_timezone(self.db_user, time_zone=time_zone)

        await self.event.answer(
            text=f'Часовой пояс изменен на "{time_zone.value.name}"',
            reply_markup=ReplyKeyboardRemove(),
        )
