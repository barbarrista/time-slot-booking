from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message
from aiogram.utils.keyboard import InlineKeyboardBuilder

from app.adapters.telegram_bot.keyboards import start_booking
from app.adapters.telegram_bot.middleware import RegisteredUserRequiredMiddleware

router = Router()
router.message.outer_middleware(RegisteredUserRequiredMiddleware())


@router.message(Command(commands=["start"]))
async def start(message: Message) -> None:
    builder = InlineKeyboardBuilder()
    builder.row(start_booking.START_BOOKING_BUTTON)
    await message.answer(
        text="Выберите действие",
        reply_markup=builder.as_markup(one_time_keyboard=True),
    )
