from aiogram.types import KeyboardButton
from aiogram.utils.keyboard import ReplyKeyboardBuilder

BUILDER = ReplyKeyboardBuilder(
    markup=[
        [KeyboardButton(text="Отправить свой номер телефона", request_contact=True)],
    ],
)
