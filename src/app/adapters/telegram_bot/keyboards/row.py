from aiogram.types import InlineKeyboardButton


class KeyboardRow:
    def __init__(self, items: list[InlineKeyboardButton] | None = None) -> None:
        self._row: list[InlineKeyboardButton] = items or []

    def append(self, item: InlineKeyboardButton) -> None:
        self._row.append(item)

    @property
    def list(self) -> list[InlineKeyboardButton]:
        return self._row
