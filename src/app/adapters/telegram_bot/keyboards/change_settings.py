from aiogram.types import InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder

from app.adapters.telegram_bot.callbacks import EditTimezoneCallback, TimezoneCallback
from app.core.timezones import TimeZone
from lib.utils import pack

EDIT_TIMEZONE_BUTTON = InlineKeyboardButton(
    text="Редактировать часовой пояс",
    callback_data=EditTimezoneCallback().pack(),
)


def get_timezone_buttons() -> list[list[InlineKeyboardButton]]:
    return pack(
        (
            InlineKeyboardButton(
                text=tz.value.name,
                callback_data=TimezoneCallback(
                    time_zone=tz.name,  # type: ignore[arg-type]
                ).pack(),
            )
            for tz in TimeZone
        ),
        size=3,
    )


BUILDER = InlineKeyboardBuilder(
    markup=[
        [EDIT_TIMEZONE_BUTTON],
    ],
)
