from aiogram.types import InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder

from app.adapters.telegram_bot.callbacks import StartBookingCallback

START_BOOKING_BUTTON = InlineKeyboardButton(
    text="Записаться на консультацию",
    callback_data=StartBookingCallback().pack(),
)

BUILDER = InlineKeyboardBuilder(
    markup=[
        [START_BOOKING_BUTTON],
    ],
)
