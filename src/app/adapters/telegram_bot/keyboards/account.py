from typing import Literal

from aiogram.types import InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder

from app.adapters.telegram_bot.callbacks import (
    PaginateReservedSlotsCallback,
    ShowReservedSlotsCallback,
)

SHOW_BOOKING_BUTTON = InlineKeyboardButton(
    text="Забронированные слоты",
    callback_data=ShowReservedSlotsCallback().pack(),
)


def get_paginate_button(
    page: int,
    *,
    direction: Literal["next", "prev"],
    without_callback: bool = False,
) -> InlineKeyboardButton:
    return InlineKeyboardButton(
        text="Вперед" if direction == "next" else "Назад",
        callback_data=(
            PaginateReservedSlotsCallback(page=page).pack()
            if not without_callback
            else None
        ),
    )


BUILDER = InlineKeyboardBuilder(
    markup=[
        [SHOW_BOOKING_BUTTON],
    ],
)
