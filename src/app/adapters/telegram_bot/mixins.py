from typing import TYPE_CHECKING, Any, Protocol, cast

from aiogram import Bot
from aiogram.types import MaybeInaccessibleMessage, Message, User

from app.adapters.telegram_bot.filters import AccountFilter
from app.db.models.telegram_user import TelegramUser


class BaseHandlerProtocol(Protocol):
    if TYPE_CHECKING:

        @property
        def bot(self) -> Bot: ...

        @property
        def message(self) -> MaybeInaccessibleMessage | None: ...

        @property
        def from_user(self) -> User: ...


class HasAccountMixin:
    data: dict[str, Any]

    @property
    def db_user(self) -> TelegramUser:
        db_user = self.data.get(AccountFilter.user_key)

        if db_user is None:
            msg = f'Key "{AccountFilter.user_key}" is not defined. May be you forgot apply {AccountFilter.__qualname__}'
            raise ValueError(msg)

        return cast(TelegramUser, db_user)


class RemoveKeyboardMixin(BaseHandlerProtocol):

    async def _remove_keyboard(
        self,
    ) -> None:
        if not isinstance(self.message, Message):
            return

        await self.bot.edit_message_reply_markup(
            chat_id=self.from_user.id,
            message_id=self.message.message_id,
            reply_markup=None,
        )
