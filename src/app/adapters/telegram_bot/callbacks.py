import datetime
from typing import Literal
from uuid import UUID

from aiogram.filters.callback_data import CallbackData


class RegistrationCallback(CallbackData, prefix="registration"):
    pass


class StartBookingCallback(CallbackData, prefix="start_booking"):
    pass


class PickDateCallback(CallbackData, prefix="pick_date"):
    date: datetime.date


class PickTimeCallback(CallbackData, prefix="pick_time"):
    slot_id: UUID


class PickSlotCallback(CallbackData, prefix="pick_slot"):
    slot_id: UUID
    is_booking_approved: bool


class EditTimezoneCallback(CallbackData, prefix="edit_tz"):
    pass


class ShowReservedSlotsCallback(CallbackData, prefix="show_reserved_slots"):
    pass


class PaginateReservedSlotsCallback(CallbackData, prefix="paginate_reserved_slots"):
    page: int


class TimezoneCallback(CallbackData, prefix="tz"):
    time_zone: Literal[
        "utc_0",
        "utc_1",
        "utc_2",
        "utc_3",
        "utc_4",
        "utc_5",
        "utc_6",
        "utc_7",
        "utc_8",
        "utc_9",
        "utc_10",
        "utc_11",
        "utc_12",
        "utc_13",
        "utc_14",
        "utc_minus_1",
        "utc_minus_2",
        "utc_minus_3",
        "utc_minus_4",
        "utc_minus_5",
        "utc_minus_6",
        "utc_minus_7",
        "utc_minus_8",
        "utc_minus_9",
        "utc_minus_10",
        "utc_minus_11",
        "utc_minus_12",
    ]


class MayBeCancelReservationCallback(CallbackData, prefix="may_be_cancel_reservation"):
    reserved_slot_id: UUID


class CancelReservationCallback(CallbackData, prefix="cancel_reservation"):
    reserved_slot_id: UUID
    is_confirmed: bool
