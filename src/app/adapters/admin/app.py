from aioinject.ext.fastapi import AioInjectMiddleware
from starlette.middleware import Middleware
from starlette_admin import I18nConfig
from starlette_admin.contrib.sqla import Admin

from app.adapters.admin.views import master, slots, telegram_user
from app.db.engine import engine
from app.db.models import (
    BookingSlot,
    Master,
    TelegramUser,
)
from app.db.models.booking import ReservedSlot
from app.di import create_container

_VIEWS = [
    master.MasterView(Master),
    telegram_user.TelegramUserView(TelegramUser),
    slots.BookingSlotView(BookingSlot, identity="booking-slot"),
    slots.ReservedSlotView(ReservedSlot, identity="reserved-slot"),
]


def create_app() -> Admin:
    app = Admin(
        engine,
        title="Админ панель",
        middlewares=[Middleware(AioInjectMiddleware, container=create_container())],
        i18n_config=I18nConfig(default_locale="ru"),
    )

    for view in _VIEWS:
        app.add_view(view)

    return app
