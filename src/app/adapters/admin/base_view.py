from typing import Any
from uuid import UUID

from starlette.requests import Request
from starlette_admin.contrib.sqla import ModelView


class UuidSerializableModelView(ModelView):
    async def get_pk_value(
        self,
        request: Request,
        obj: Any,  # noqa: ANN401
    ) -> Any:  # noqa: ANN401
        pk = await super().get_pk_value(request, obj)
        if isinstance(pk, UUID):
            return str(pk)
        return pk
