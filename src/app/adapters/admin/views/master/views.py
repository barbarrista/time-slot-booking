from collections.abc import Sequence
from typing import Any

from starlette_admin.contrib.sqla import ModelView
from starlette_admin.fields import EnumField, StringField

from app.core.timezones import TimeZone


class MasterView(ModelView):
    label = "Мастер"
    name = "Мастер"
    fields: Sequence[Any] = [
        StringField("id", label="ID"),
        StringField("is_active", label="Активен"),
        EnumField("timezone", label="Таймзона", choices=TimeZone.items()),
    ]
    exclude_fields_from_list: Sequence[str] = ["user_id"]
