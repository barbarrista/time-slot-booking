from collections.abc import Sequence
from typing import Any

from starlette_admin.contrib.sqla import ModelView
from starlette_admin.fields import DateTimeField, EnumField, StringField

from app.core.timezones import TimeZone


class TelegramUserView(ModelView):
    label = "Telegram пользователь"
    fields: Sequence[Any] = [
        StringField("id", label="ID", exclude_from_list=True),
        StringField("first_name", label="Фамилия"),
        StringField("last_name", label="Имя"),
        StringField("phone_number", label="Номер телефона"),
        StringField("is_active", label="Активен"),
        StringField("is_verified", label="Верифицирован"),
        DateTimeField("created_at", label="Создан"),
        EnumField("timezone", label="Таймзона", choices=TimeZone.items()),
    ]
