from collections.abc import Sequence
from typing import Any
from uuid import UUID

from fastapi import Request
from sqlalchemy.orm import joinedload
from sqlalchemy.sql import Select
from starlette_admin import row_action
from starlette_admin.fields import (
    BooleanField,
    DateTimeField,
    HasOne,
    StringField,
)

from app.adapters.admin.base_view import UuidSerializableModelView
from app.adapters.admin.constants import OUTPUT_DATETIME_FORMAT
from app.core.booking.repository import (
    BookingRepository,
    BookingSlotQueryBuilder,
    ReservedSlotQueryBuilder,
)
from app.db.models.booking import BookingSlot, ReservedSlot


class BookingSlotView(UuidSerializableModelView):

    label = "Тайм Слот"
    name = "Тайм Слот"
    fields: Sequence[Any] = [
        StringField("id", label="ID", exclude_from_list=True),
        DateTimeField(
            "start_time",
            label="Начало",
            output_format=OUTPUT_DATETIME_FORMAT,
        ),
        DateTimeField(
            "end_time",
            label="Окончание",
            output_format=OUTPUT_DATETIME_FORMAT,
        ),
        BooleanField("is_available", label="Доступен"),
    ]
    exclude_fields_from_list: Sequence[str] = ["master_id"]

    def get_list_query(self) -> Select[tuple[BookingSlot]]:
        return BookingSlotQueryBuilder.get_list_stmt()

    async def find_by_pk(self, request: Request, pk: UUID) -> BookingSlot | None:
        repository = BookingRepository(session=request.state.session)
        model = await repository.get_booking_slot(
            pk,
            options=(joinedload(BookingSlot.master),),
        )
        if model is not None:
            model.apply_master_timezone()

        return model

    async def find_all(  # noqa: PLR0913
        self,
        request: Request,
        skip: int = 0,
        limit: int = 100,
        where: dict[str, Any] | str | None = None,
        order_by: list[str] | None = None,
    ) -> Sequence[BookingSlot]:
        models: Sequence[BookingSlot] = await super().find_all(
            request,
            skip,
            limit,
            where,
            order_by,
        )

        for model in models:
            model.start_time = model.start_time + model.master.tz_delta
            model.end_time = model.end_time + model.master.tz_delta

        return models

    async def repr(self, obj: BookingSlot, _: Request) -> str:
        return obj.represent_datetime(obj.master.timezone, show_tz=True)


class ReservedSlotView(UuidSerializableModelView):

    label = "Забронированный Тайм Слот"
    name = "Забронированный Тайм Слот"

    fields: Sequence[Any] = [
        StringField("id", label="ID", exclude_from_list=True),
        HasOne("booking_slot", label="Слот", identity="booking-slot"),
        BooleanField("is_deleted", label="Акрутален?"),
        BooleanField("is_approved", label="Подтверждён?"),
    ]

    @row_action(
        name="cancel_reservation",
        text="Отменить бронь",
        confirmation="Вы уверены, что хотите отменить бронь?",
        submit_btn_text="Да, отменить",
        submit_btn_class="btn-success",
        action_btn_class="btn-danger",
        form="""
        <form>
            <div class="mt-3">
                <input type="text" class="form-control" name="example-text-input" placeholder="Причина отмены">
            </div>
        </form>
        """,
    )
    async def cancel_reservation(
        self,
        request: Request,  # noqa: ARG002
        pk: UUID,  # noqa: ARG002
    ) -> str:
        return "Бронирование отменено"

    def get_list_query(self) -> Select[tuple[ReservedSlot]]:
        return ReservedSlotQueryBuilder.get_list_stmt()

    async def find_by_pk(self, request: Request, pk: UUID) -> ReservedSlot | None:
        repository = BookingRepository(session=request.state.session)
        return await repository.get_reserved_slot_by_id(
            pk,
            options=(
                joinedload(ReservedSlot.booking_slot).selectinload(BookingSlot.master),
            ),
        )
