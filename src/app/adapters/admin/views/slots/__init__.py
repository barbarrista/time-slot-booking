from .views import BookingSlotView, ReservedSlotView

__all__ = ["BookingSlotView", "ReservedSlotView"]
