from lib.schemas import BaseSchema


class APIErrorSchema(BaseSchema):
    code: str
    message: str
