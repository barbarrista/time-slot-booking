from fastapi.encoders import jsonable_encoder
from fastapi.requests import Request
from fastapi.responses import JSONResponse

from .exceptions import BaseHTTPError


async def http_exception_handler(
    request: Request,  # noqa: ARG001
    exc: BaseHTTPError,
) -> JSONResponse:
    return JSONResponse(
        content=jsonable_encoder(exc.error_schema.model_dump(by_alias=True)),
        status_code=exc.status_code,
    )
