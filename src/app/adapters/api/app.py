import contextlib
from collections.abc import AsyncIterator
from contextlib import aclosing

from aioinject.ext.fastapi import AioInjectMiddleware
from fastapi import APIRouter, FastAPI
from starlette.middleware.cors import CORSMiddleware

from app import telemetry
from app.adapters import admin
from app.di import create_container
from app.settings import AppSettings
from lib.settings import get_settings

from .exception_handlers import http_exception_handler
from .exceptions import BaseHTTPError

_routers: list[APIRouter] = []


@contextlib.asynccontextmanager
async def _lifespan(
    app: FastAPI,  # noqa: ARG001 - required by lifespan protocol
) -> AsyncIterator[None]:  # pragma: no cover
    async with aclosing(create_container()):
        yield


def create_app() -> FastAPI:
    telemetry.setup_telemetry()

    app = FastAPI(lifespan=_lifespan)

    for router in _routers:
        app.include_router(router)

    _add_exception_handlers(app=app)
    _add_middlewares(app=app)

    @app.get("/health")
    async def healthcheck() -> None:
        return None

    admin_app = admin.create_app()
    admin_app.mount_to(app)
    return app


def _add_exception_handlers(app: FastAPI) -> None:
    app.exception_handlers[BaseHTTPError] = http_exception_handler


def _add_middlewares(app: FastAPI) -> None:
    app_settings = get_settings(AppSettings)

    app.add_middleware(AioInjectMiddleware, container=create_container())
    app.add_middleware(
        CORSMiddleware,
        allow_origins=app_settings.allow_origins,
        allow_origin_regex=app_settings.allow_origin_regex,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
