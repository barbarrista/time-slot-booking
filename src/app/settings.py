import enum
from urllib.parse import quote_plus

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class LoggingLevel(enum.StrEnum):
    DEBUG = enum.auto()
    INFO = enum.auto()
    WARNING = enum.auto()
    ERROR = enum.auto()
    CRITICAL = enum.auto()


class DatabaseSettings(BaseSettings):
    model_config = SettingsConfigDict(str_strip_whitespace=True, env_prefix="database_")

    driver: str = "postgresql+asyncpg"
    name: str
    username: str
    password: str
    host: str

    echo: bool = False

    @property
    def url(self) -> str:
        password = quote_plus(self.password)
        return f"{self.driver}://{self.username}:{password}@{self.host}/{self.name}"


class SentrySettings(BaseSettings):
    model_config = SettingsConfigDict(str_strip_whitespace=True, env_prefix="sentry_")

    dsn: str = ""
    environment: str = "production"
    traces_sample_rate: float = Field(default=1.0, ge=0.0, le=1.0)


class AppSettings(BaseSettings):
    model_config = SettingsConfigDict(str_strip_whitespace=True, env_prefix="app_")
    unsafe_debug: bool = False

    allow_origins: list[str] = []
    allow_origin_regex: str | None = None
    logging_level: LoggingLevel = LoggingLevel.INFO
    default_page_size: int = 7


class TelegramSettings(BaseSettings):
    model_config = SettingsConfigDict(
        str_strip_whitespace=True,
        env_prefix="telegram_bot_",
    )

    token: str
