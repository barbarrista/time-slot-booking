import aioinject

from app.core.master import MasterRepository
from lib.types import Providers

PROVIDERS: Providers = [
    aioinject.Scoped(MasterRepository),
]
