import aioinject

from app.core.pagination import PagePaginator
from lib.types import Providers

PROVIDERS: Providers = [aioinject.Scoped(PagePaginator)]
