import aioinject

from app.core.booking import BookingRepository, BookingSlotQueryBuilder
from app.core.booking.commands import CancelSlotReservationCommand, ReserveSlotCommand
from app.core.booking.queries import FreeDateSlotsQuery, ReservedSlotsQuery
from lib.types import Providers

PROVIDERS: Providers = [
    aioinject.Scoped(BookingRepository),
    aioinject.Scoped(BookingSlotQueryBuilder),
    aioinject.Scoped(ReserveSlotCommand),
    aioinject.Scoped(FreeDateSlotsQuery),
    aioinject.Scoped(ReservedSlotsQuery),
    aioinject.Scoped(CancelSlotReservationCommand),
]
