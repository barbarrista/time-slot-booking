import aioinject
from jinja2 import Environment

from app.adapters.telegram_bot import templates
from app.core.markdown.service import MarkdownService
from lib.types import Providers

PROVIDERS: Providers = [
    aioinject.Object(templates.env, type_=Environment),
    aioinject.Scoped(MarkdownService),
]
