import aioinject

from app.core.telegram_user import TelegramUserRepository
from lib.types import Providers

PROVIDERS: Providers = [
    aioinject.Scoped(TelegramUserRepository),
]
