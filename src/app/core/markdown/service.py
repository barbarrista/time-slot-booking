from jinja2 import Environment


class MarkdownService:
    def __init__(self, env: Environment) -> None:
        self.env = env

    def show_booking_slots_info(self, tz_name: str) -> str:
        template = self.env.get_template("show_booking_slots_info.md")
        return template.render(tz_name=tz_name)
