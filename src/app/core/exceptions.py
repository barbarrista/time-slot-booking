from uuid import UUID


class ObjectNotFoundError(Exception):
    def __init__(self, ident: UUID | int) -> None:
        self.ident = str(ident)
