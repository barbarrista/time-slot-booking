from result import Err, Ok, Result

from app.core.booking.dto import (
    MayBeReserveSlotDTO,
    ReservedSlotFilterDTO,
    ReserveSlotDTO,
)
from app.core.booking.exceptions import SlotAlreadyReservedError
from app.core.booking.repository import BookingRepository
from app.core.exceptions import ObjectNotFoundError
from app.core.master.dto import MasterFilterDTO
from app.core.master.repository import MasterRepository
from app.db.models.booking import ReservedSlot
from lib.utils import utc_now


class ReserveSlotCommand:
    def __init__(
        self,
        repository: BookingRepository,
        master_repository: MasterRepository,
    ) -> None:
        self._repository = repository
        self._master_repository = master_repository

    async def execute(
        self,
        dto: MayBeReserveSlotDTO,
    ) -> Result[ReservedSlot, ObjectNotFoundError | SlotAlreadyReservedError]:
        await self._master_repository.get(
            dto=MasterFilterDTO(slot_id=dto.slot_id),
            with_for_update=True,
        )

        slot = await self._repository.get_booking_slot(dto.slot_id)
        if slot is None:
            return Err(ObjectNotFoundError(dto.slot_id))

        if (
            await self._repository.get_reserved_slot(
                dto=ReservedSlotFilterDTO(slot_id=dto.slot_id),
                time_offset=dto.user.tz_delta,
                start_time_gte=utc_now(),
            )
            is not None
        ):
            return Err(SlotAlreadyReservedError())

        return Ok(
            await self._repository.reserve_slot(
                dto=ReserveSlotDTO(slot=slot, user=dto.user),
            ),
        )
