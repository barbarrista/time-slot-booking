from result import Err, Ok, Result

from app.core.booking.dto import (
    MayBeCancelSlotReservationDTO,
    ReservedSlotFilterDTO,
)
from app.core.booking.repository import BookingRepository
from app.core.exceptions import ObjectNotFoundError
from app.core.master.dto import MasterFilterDTO
from app.core.master.repository import MasterRepository
from app.db.models.booking import ReservedSlot
from lib.utils import utc_now


class CancelSlotReservationCommand:
    def __init__(
        self,
        repository: BookingRepository,
        master_repository: MasterRepository,
    ) -> None:
        self._repository = repository
        self._master_repository = master_repository

    async def execute(
        self,
        dto: MayBeCancelSlotReservationDTO,
    ) -> Result[ReservedSlot, ObjectNotFoundError]:
        await self._master_repository.get(
            dto=MasterFilterDTO(reserved_slot_id=dto.reserved_slot_id),
            with_for_update=True,
        )

        slot = await self._repository.get_reserved_slot(
            dto=ReservedSlotFilterDTO(id=dto.reserved_slot_id),
            time_offset=dto.user.tz_delta,
            start_time_gte=utc_now(),
        )
        if slot is None:
            return Err(ObjectNotFoundError(dto.reserved_slot_id))

        return Ok(await self._repository.cancel_slot_reservation(slot))
