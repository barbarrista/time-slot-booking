import datetime
from collections.abc import Sequence
from uuid import UUID

from sqlalchemy import Date, Select, cast, or_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.interfaces import ORMOption

from app.core.booking.dto import (
    ReservedSlotFilterDTO,
    ReserveSlotDTO,
)
from app.db.models.booking import BookingSlot, ReservedSlot


class BookingRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get_booking_slot(
        self,
        ident: UUID,
        options: Sequence[ORMOption] | None = None,
    ) -> BookingSlot | None:
        return await self._session.get(BookingSlot, ident, options=options)

    async def get_free_date_slots(
        self,
        start_time_gte: datetime.datetime,
    ) -> Sequence[datetime.date]:
        date = cast(BookingSlot.start_time, Date)
        stmt = (
            select(date)
            .join(BookingSlot.reserved_slot, isouter=True)
            .where(
                BookingSlot.start_time >= start_time_gte,
                or_(
                    ReservedSlot.telegram_user_id.is_(None),
                    ReservedSlot.is_deleted.is_(False),
                ),
            )
            .order_by(date)
            .distinct()
        )
        return (await self._session.scalars(stmt)).all()

    async def get_free_slots(
        self,
        booking_date: datetime.date,
        start_time_gte: datetime.datetime,
    ) -> Sequence[BookingSlot]:
        date = cast(BookingSlot.start_time, Date)
        sub_stmt = (
            select(ReservedSlot.slot_id)
            .join(ReservedSlot.booking_slot)
            .where(
                ReservedSlot.is_deleted.is_(other=False),
            )
        )
        stmt = (
            select(BookingSlot)
            .join(BookingSlot.reserved_slot, isouter=True)
            .where(
                date == booking_date,
                or_(
                    BookingSlot.id.not_in(sub_stmt),
                    ReservedSlot.telegram_user_id.is_(None),
                ),
                BookingSlot.is_available.is_(True),
                BookingSlot.start_time >= start_time_gte,
            )
            .order_by(
                BookingSlot.start_time,
            )
        )
        return (await self._session.scalars(stmt)).all()

    async def reserve_slot(self, dto: ReserveSlotDTO) -> ReservedSlot:
        model = ReservedSlot(slot_id=dto.slot.id, telegram_user_id=dto.user.id)
        self._session.add(model)
        await self._session.flush()
        return model

    async def cancel_slot_reservation(self, slot: ReservedSlot) -> ReservedSlot:
        slot.is_deleted = True
        self._session.add(slot)
        await self._session.flush()
        return slot

    async def get_reserved_slot(
        self,
        dto: ReservedSlotFilterDTO,
        *,
        time_offset: datetime.timedelta,
        start_time_gte: datetime.datetime,
    ) -> ReservedSlot | None:
        stmt = self.get_reserved_slots_stmt(
            dto,
            time_offset=time_offset,
            start_time_gte=start_time_gte,
        )
        stmt = self._apply_filter(stmt, dto=dto)
        return (await self._session.scalars(stmt)).one_or_none()

    def get_reserved_slots_stmt(
        self,
        dto: ReservedSlotFilterDTO,
        *,
        time_offset: datetime.timedelta,
        start_time_gte: datetime.datetime,
    ) -> Select[tuple[ReservedSlot]]:
        stmt = (
            select(ReservedSlot)
            .where(
                BookingSlot.start_time >= start_time_gte + time_offset,
                ReservedSlot.is_deleted.is_(other=False),
            )
            .join(ReservedSlot.booking_slot)
            .order_by(BookingSlot.start_time)
            .options(joinedload(ReservedSlot.booking_slot))
        )
        return self._apply_filter(stmt, dto=dto)

    @staticmethod
    def _apply_filter(
        stmt: Select[tuple[ReservedSlot]],
        *,
        dto: ReservedSlotFilterDTO,
    ) -> Select[tuple[ReservedSlot]]:
        if dto.id is not None:
            stmt = stmt.where(ReservedSlot.id == dto.id)

        if dto.telegram_user_id is not None:
            stmt = stmt.where(ReservedSlot.telegram_user_id == dto.telegram_user_id)

        if dto.slot_id is not None:
            stmt = stmt.where(ReservedSlot.slot_id == dto.slot_id)

        return stmt

    async def get_reserved_slot_by_id(
        self,
        ident: UUID,
        options: Sequence[ORMOption] | None = None,
    ) -> ReservedSlot | None:
        return await self._session.get(ReservedSlot, ident, options=options)


class BookingSlotQueryBuilder:
    @staticmethod
    def get_list_stmt() -> Select[tuple[BookingSlot]]:
        return select(BookingSlot).options(joinedload(BookingSlot.master))


class ReservedSlotQueryBuilder:
    @staticmethod
    def get_list_stmt() -> Select[tuple[ReservedSlot]]:
        return select(ReservedSlot).options(
            joinedload(ReservedSlot.booking_slot).joinedload(BookingSlot.master),
        )
