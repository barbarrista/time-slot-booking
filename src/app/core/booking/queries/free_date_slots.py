from collections.abc import Sequence
from datetime import date

from result import Err, Ok, Result

from app.core.booking.dto import FreeDateSlotsDTO
from app.core.booking.repository import BookingRepository
from app.core.exceptions import ObjectNotFoundError
from app.core.master.dto import MasterFilterDTO
from app.core.master.repository import MasterRepository


class FreeDateSlotsQuery:
    def __init__(
        self,
        repository: BookingRepository,
        master_repository: MasterRepository,
    ) -> None:
        self._repository = repository
        self._master_repository = master_repository

    async def execute(
        self,
        dto: FreeDateSlotsDTO,
    ) -> Result[Sequence[date], ObjectNotFoundError]:
        master = await self._master_repository.get(
            dto=MasterFilterDTO(ident=dto.master_id),
        )
        if master is None:
            return Err(ObjectNotFoundError(ident=dto.master_id))

        return Ok(
            await self._repository.get_free_date_slots(
                start_time_gte=dto.start_time_gte + dto.user.timezone.value.delta,
            ),
        )
