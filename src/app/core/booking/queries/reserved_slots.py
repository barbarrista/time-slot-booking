from app.core.booking.dto import ReservedSlotFilterDTO
from app.core.booking.repository import BookingRepository
from app.core.dto import PaginationDTO, PaginationResultDTO
from app.core.pagination import PagePaginator
from app.db.models.booking import ReservedSlot
from app.db.models.telegram_user import TelegramUser
from app.settings import AppSettings
from lib.utils import utc_now


class ReservedSlotsQuery:
    def __init__(
        self,
        repository: BookingRepository,
        paginator: PagePaginator,
        app_settings: AppSettings,
    ) -> None:
        self._repository = repository
        self._paginator = paginator
        self._app_settings = app_settings

    async def execute(
        self,
        user: TelegramUser,
        page: int,
    ) -> PaginationResultDTO[ReservedSlot]:
        stmt = self._repository.get_reserved_slots_stmt(
            dto=ReservedSlotFilterDTO(telegram_user_id=user.id),
            time_offset=user.tz_delta,
            start_time_gte=utc_now(),
        )

        return await self._paginator.paginate(
            stmt,
            pagination=PaginationDTO(
                page=page,
                page_size=self._app_settings.default_page_size,
            ),
        )
