from .free_date_slots import FreeDateSlotsQuery
from .reserved_slots import ReservedSlotsQuery

__all__ = ["FreeDateSlotsQuery", "ReservedSlotsQuery"]
