from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from app.db.models.booking import BookingSlot
from app.db.models.telegram_user import TelegramUser


@dataclass(frozen=True, slots=True)
class MayBeReserveSlotDTO:
    slot_id: UUID
    user: TelegramUser


@dataclass(frozen=True, slots=True)
class ReserveSlotDTO:
    slot: BookingSlot
    user: TelegramUser


@dataclass(frozen=True, slots=True)
class FreeDateSlotsDTO:
    master_id: UUID
    start_time_gte: datetime
    user: TelegramUser


@dataclass(frozen=True, slots=True)
class ReservedSlotFilterDTO:
    id: UUID | None = None
    telegram_user_id: int | None = None
    slot_id: UUID | None = None


@dataclass(frozen=True, slots=True)
class MayBeCancelSlotReservationDTO:
    reserved_slot_id: UUID
    user: TelegramUser


@dataclass(frozen=True, slots=True)
class CancelSlotReservationDTO:
    slot: BookingSlot
    user: TelegramUser
