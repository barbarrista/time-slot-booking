import enum
from collections.abc import Sequence
from dataclasses import dataclass
from datetime import timedelta


@dataclass(frozen=True, slots=True)
class TimeZoneInfo:
    name: str
    delta: timedelta


class TimeZone(enum.Enum):
    utc_0 = TimeZoneInfo(name="UTC+0", delta=timedelta(hours=0))
    utc_1 = TimeZoneInfo(name="UTC+1", delta=timedelta(hours=1))
    utc_2 = TimeZoneInfo(name="UTC+2", delta=timedelta(hours=2))
    utc_3 = TimeZoneInfo(name="UTC+3", delta=timedelta(hours=3))
    utc_4 = TimeZoneInfo(name="UTC+4", delta=timedelta(hours=4))
    utc_5 = TimeZoneInfo(name="UTC+5", delta=timedelta(hours=5))
    utc_6 = TimeZoneInfo(name="UTC+6", delta=timedelta(hours=6))
    utc_7 = TimeZoneInfo(name="UTC+7", delta=timedelta(hours=7))
    utc_8 = TimeZoneInfo(name="UTC+8", delta=timedelta(hours=8))
    utc_9 = TimeZoneInfo(name="UTC+9", delta=timedelta(hours=9))
    utc_10 = TimeZoneInfo(name="UTC+10", delta=timedelta(hours=10))
    utc_11 = TimeZoneInfo(name="UTC+11", delta=timedelta(hours=11))
    utc_12 = TimeZoneInfo(name="UTC+12", delta=timedelta(hours=12))
    utc_13 = TimeZoneInfo(name="UTC+13", delta=timedelta(hours=13))
    utc_14 = TimeZoneInfo(name="UTC+14", delta=timedelta(hours=14))

    utc_minus_1 = TimeZoneInfo(name="UTC-1", delta=timedelta(hours=-1))
    utc_minus_2 = TimeZoneInfo(name="UTC-2", delta=timedelta(hours=-2))
    utc_minus_3 = TimeZoneInfo(name="UTC-3", delta=timedelta(hours=-3))
    utc_minus_4 = TimeZoneInfo(name="UTC-4", delta=timedelta(hours=-4))
    utc_minus_5 = TimeZoneInfo(name="UTC-5", delta=timedelta(hours=-5))
    utc_minus_6 = TimeZoneInfo(name="UTC-6", delta=timedelta(hours=-6))
    utc_minus_7 = TimeZoneInfo(name="UTC-7", delta=timedelta(hours=-7))
    utc_minus_8 = TimeZoneInfo(name="UTC-8", delta=timedelta(hours=-8))
    utc_minus_9 = TimeZoneInfo(name="UTC-9", delta=timedelta(hours=-9))
    utc_minus_10 = TimeZoneInfo(name="UTC-10", delta=timedelta(hours=-10))
    utc_minus_11 = TimeZoneInfo(name="UTC-11", delta=timedelta(hours=-11))
    utc_minus_12 = TimeZoneInfo(name="UTC-12", delta=timedelta(hours=-12))

    @classmethod
    def items(cls) -> Sequence[tuple[TimeZoneInfo, str]]:
        return [(enum_.value, enum_.value.name) for enum_ in cls]
