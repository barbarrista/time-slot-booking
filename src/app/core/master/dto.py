from dataclasses import dataclass
from uuid import UUID


@dataclass(frozen=True, slots=True)
class MasterFilterDTO:
    ident: UUID | None = None
    slot_id: UUID | None = None
    reserved_slot_id: UUID | None = None
