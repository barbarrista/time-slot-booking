from sqlalchemy import Select, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.master.dto import MasterFilterDTO
from app.db.models.booking import BookingSlot, ReservedSlot
from app.db.models.master import Master


class MasterRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(
        self,
        dto: MasterFilterDTO,
        *,
        with_for_update: bool = False,
    ) -> Master | None:
        stmt = select(Master)
        stmt = self._apply_filters(stmt, dto=dto)

        if with_for_update:
            stmt = stmt.with_for_update()

        return (await self._session.scalars(stmt)).one_or_none()

    @staticmethod
    def _apply_filters(
        stmt: Select[tuple[Master]],
        *,
        dto: MasterFilterDTO,
    ) -> Select[tuple[Master]]:
        is_booking_slots_joined = False
        if dto.ident is not None:
            stmt = stmt.where(Master.id == dto.ident)

        if dto.slot_id is not None:
            stmt = stmt.join(Master.booking_slots).where(BookingSlot.id == dto.slot_id)
            is_booking_slots_joined = True

        if dto.reserved_slot_id is not None:
            if not is_booking_slots_joined:
                stmt = stmt.join(Master.booking_slots)

            stmt = stmt.join(
                ReservedSlot,
                ReservedSlot.slot_id == BookingSlot.id,
            ).where(ReservedSlot.id == dto.reserved_slot_id)

        return stmt
