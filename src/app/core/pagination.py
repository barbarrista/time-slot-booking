from typing import TypeVar

from sqlalchemy import Select, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.dto import PaginationDTO, PaginationResultDTO

_T = TypeVar("_T")


class PagePaginator:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def paginate(
        self,
        stmt: Select[tuple[_T]],
        *,
        pagination: PaginationDTO,
    ) -> PaginationResultDTO[_T]:
        paginated = stmt.offset((pagination.page - 1) * pagination.page_size).limit(
            pagination.page_size,
        )

        count = (
            await self._session.scalar(
                select(func.count()).select_from(stmt.subquery()),
            )
            or 0
        )
        items = (await self._session.scalars(paginated)).unique().all()
        return PaginationResultDTO(
            items=items,
            has_next_page=pagination.page * pagination.page_size < count,
            count=count,
        )
