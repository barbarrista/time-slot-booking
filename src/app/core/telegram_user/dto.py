from dataclasses import dataclass


@dataclass
class TelegramUserCreateDTO:
    id: int
    first_name: str
    last_name: str | None


@dataclass
class TelegramUserUpdateDTO:
    first_name: str
    last_name: str | None
    phone_number: str
    is_verified: bool
