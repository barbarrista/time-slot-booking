from sqlalchemy.ext.asyncio import AsyncSession

from app.core.telegram_user.dto import TelegramUserCreateDTO, TelegramUserUpdateDTO
from app.core.timezones import TimeZone
from app.db.models import TelegramUser


class TelegramUserRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(self, id_: int) -> TelegramUser | None:
        return await self._session.get(TelegramUser, id_)

    async def create(self, dto: TelegramUserCreateDTO) -> TelegramUser:
        user = TelegramUser(
            id=dto.id,
            first_name=dto.first_name,
            last_name=dto.last_name,
            is_active=True,
            is_verified=False,
            phone_number=None,
            timezone=TimeZone.utc_3,
        )
        self._session.add(user)
        await self._session.flush()
        return user

    async def update(
        self,
        user: TelegramUser,
        dto: TelegramUserUpdateDTO,
    ) -> TelegramUser:
        user.first_name = dto.first_name
        user.last_name = dto.last_name or user.last_name
        user.phone_number = dto.phone_number
        user.is_verified = dto.is_verified
        self._session.add(user)
        await self._session.flush()
        return user

    async def set_timezone(
        self,
        user: TelegramUser,
        *,
        time_zone: TimeZone,
    ) -> TelegramUser:
        user.timezone = time_zone
        self._session.add(user)
        await self._session.flush()
        return user
