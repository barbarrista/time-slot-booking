from collections.abc import Sequence
from dataclasses import dataclass
from typing import Generic, TypeVar

from pydantic import BaseModel, ConfigDict

_T = TypeVar("_T")


class BaseDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)


@dataclass(frozen=True, slots=True)
class PaginationDTO:
    page: int
    page_size: int


@dataclass(frozen=True, slots=True)
class PaginationResultDTO(Generic[_T]):
    items: Sequence[_T]
    has_next_page: bool
    count: int
