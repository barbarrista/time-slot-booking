from datetime import datetime

from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column

from app.db import Base, str_64, str_128, str_320, uuid_pk
from lib.utils import utc_now


class User(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "user"

    id: Mapped[uuid_pk] = mapped_column(init=False)
    first_name: Mapped[str_128 | None]
    last_name: Mapped[str_128 | None]
    middle_name: Mapped[str_128 | None]

    email: Mapped[str_320] = mapped_column(unique=True)
    username: Mapped[str_64] = mapped_column(unique=True)
    password_hash: Mapped[str]

    is_active: Mapped[bool] = mapped_column(default=True)
    is_confirmed: Mapped[bool] = mapped_column(default=False)

    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
    updated_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
