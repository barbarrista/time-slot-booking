from datetime import timedelta
from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped

from app.core.timezones import TimeZone


class TimeZoneMixin:
    if TYPE_CHECKING:
        timezone: Mapped[TimeZone]

    @property
    def tz_name(self) -> str:
        return self.timezone.value.name

    @property
    def tz_delta(self) -> timedelta:
        return self.timezone.value.delta
