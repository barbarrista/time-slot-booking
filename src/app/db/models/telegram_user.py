from datetime import datetime

from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column

from app.core.timezones import TimeZone
from app.db import Base, int64, str_16, str_128
from app.db.models.mixins import TimeZoneMixin
from lib.utils import utc_now


class TelegramUser(MappedAsDataclass, TimeZoneMixin, Base, kw_only=True):
    __tablename__ = "telegram_user"

    id: Mapped[int64]
    first_name: Mapped[str_128]
    last_name: Mapped[str_128 | None]
    is_active: Mapped[bool] = mapped_column(default_factory=lambda: True)
    is_verified: Mapped[bool] = mapped_column(default_factory=lambda: False)
    phone_number: Mapped[str_16 | None]
    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
    timezone: Mapped[TimeZone]

    @property
    def has_phone_number(self) -> bool:
        return self.phone_number is not None
