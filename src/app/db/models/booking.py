from datetime import datetime, timedelta
from uuid import UUID

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship

from app.core.timezones import TimeZone
from app.db import Base, uuid_pk
from app.db.models.master import Master
from app.db.models.telegram_user import TelegramUser
from lib.utils import represent_date, represent_time, utc_now


class ReservedSlot(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "reserved_slot"

    id: Mapped[uuid_pk] = mapped_column(init=False)
    slot_id: Mapped[UUID] = mapped_column(
        ForeignKey("booking_slot.id", ondelete="CASCADE"),
    )
    booking_slot: Mapped["BookingSlot"] = relationship(
        back_populates="reserved_slot",
        init=False,
    )
    telegram_user_id: Mapped[int] = mapped_column(
        ForeignKey("telegram_user.id", ondelete="CASCADE"),
    )
    telegram_user: Mapped[TelegramUser] = relationship(init=False)
    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
    is_deleted: Mapped[bool] = mapped_column(default=False)
    is_approved: Mapped[bool] = mapped_column(default=False)


class BookingSlot(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "booking_slot"

    id: Mapped[uuid_pk] = mapped_column(init=False)
    start_time: Mapped[datetime]
    end_time: Mapped[datetime]
    is_available: Mapped[bool] = mapped_column(default_factory=lambda: True)
    master_id: Mapped[UUID] = mapped_column(ForeignKey("master.id"))
    master: Mapped[Master] = relationship(init=False, back_populates="booking_slots")
    reserved_slot: Mapped[ReservedSlot | None] = relationship(
        init=False,
        back_populates="booking_slot",
    )

    def represent_time_period(self, delta: timedelta) -> str:
        time_from = represent_time(self.start_time, delta=delta)
        time_to = represent_time(self.end_time, delta=delta)
        return f"{time_from} - {time_to}"

    def represent_datetime(self, tz: TimeZone, *, show_tz: bool = False) -> str:
        time_period = self.represent_time_period(tz.value.delta)
        represented = f"{represent_date(self.start_time)} {time_period}"
        if show_tz:
            return f"{represented} ({tz.value.name})"

        return represented

    def apply_master_timezone(self) -> None:
        # Warning!!! Use joinedload BookingSlot.master previously!
        self.start_time = self.start_time + self.master.tz_delta
        self.end_time = self.end_time + self.master.tz_delta
