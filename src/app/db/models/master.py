from typing import TYPE_CHECKING
from uuid import UUID

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship

from app.core.timezones import TimeZone
from app.db import Base, uuid_pk
from app.db.models.mixins import TimeZoneMixin

if TYPE_CHECKING:
    from app.db.models.booking import BookingSlot

    from .user import User


class Master(MappedAsDataclass, TimeZoneMixin, Base, kw_only=True):
    __tablename__ = "master"

    id: Mapped[uuid_pk] = mapped_column(init=False)
    user_id: Mapped[UUID] = mapped_column(
        ForeignKey("user.id", ondelete="CASCADE"),
        init=False,
        unique=True,
    )
    user: Mapped["User"] = relationship()
    is_active: Mapped[bool] = mapped_column(default=True)
    timezone: Mapped[TimeZone]
    booking_slots: Mapped[list["BookingSlot"]] = relationship(
        default_factory=list,
        back_populates="master",
    )
