"""
Initial

Revision ID: 015d8f17df31
Revises:
Create Date: 2024-02-18 17:05:31.044896

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "015d8f17df31"
down_revision: str | None = None
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "telegram_user",
        sa.Column("id", sa.BigInteger(), autoincrement=False, nullable=False),
        sa.Column("first_name", sa.String(), nullable=False),
        sa.Column("last_name", sa.String(), nullable=True),
        sa.Column("is_active", sa.Boolean(), nullable=False),
        sa.Column("is_verified", sa.Boolean(), nullable=False),
        sa.Column("phone_number", sa.String(), nullable=True),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.Column(
            "timezone",
            sa.Enum(
                "utc_0",
                "utc_1",
                "utc_2",
                "utc_3",
                "utc_4",
                "utc_5",
                "utc_6",
                "utc_7",
                "utc_8",
                "utc_9",
                "utc_10",
                "utc_11",
                "utc_12",
                "utc_13",
                "utc_14",
                "utc_minus_1",
                "utc_minus_2",
                "utc_minus_3",
                "utc_minus_4",
                "utc_minus_5",
                "utc_minus_6",
                "utc_minus_7",
                "utc_minus_8",
                "utc_minus_9",
                "utc_minus_10",
                "utc_minus_11",
                "utc_minus_12",
                name="timezone",
                native_enum=False,
            ),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_telegram_user")),
    )
    op.create_table(
        "user",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("first_name", sa.String(), nullable=True),
        sa.Column("last_name", sa.String(), nullable=True),
        sa.Column("middle_name", sa.String(), nullable=True),
        sa.Column("email", sa.String(), nullable=False),
        sa.Column("username", sa.String(), nullable=False),
        sa.Column("password_hash", sa.String(), nullable=False),
        sa.Column("is_active", sa.Boolean(), nullable=False),
        sa.Column("is_confirmed", sa.Boolean(), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_user")),
        sa.UniqueConstraint("email", name=op.f("uq_user_email")),
        sa.UniqueConstraint("username", name=op.f("uq_user_username")),
    )
    op.create_table(
        "master",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("user_id", sa.Uuid(), nullable=False),
        sa.Column("is_active", sa.Boolean(), nullable=False),
        sa.Column(
            "timezone",
            sa.Enum(
                "utc_0",
                "utc_1",
                "utc_2",
                "utc_3",
                "utc_4",
                "utc_5",
                "utc_6",
                "utc_7",
                "utc_8",
                "utc_9",
                "utc_10",
                "utc_11",
                "utc_12",
                "utc_13",
                "utc_14",
                "utc_minus_1",
                "utc_minus_2",
                "utc_minus_3",
                "utc_minus_4",
                "utc_minus_5",
                "utc_minus_6",
                "utc_minus_7",
                "utc_minus_8",
                "utc_minus_9",
                "utc_minus_10",
                "utc_minus_11",
                "utc_minus_12",
                name="timezone",
                native_enum=False,
            ),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
            name=op.f("fk_master_user_id_user"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_master")),
        sa.UniqueConstraint("user_id", name=op.f("uq_master_user_id")),
    )
    op.create_table(
        "booking_slot",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("start_time", sa.DateTime(timezone=True), nullable=False),
        sa.Column("end_time", sa.DateTime(timezone=True), nullable=False),
        sa.Column("is_available", sa.Boolean(), nullable=False),
        sa.Column("master_id", sa.Uuid(), nullable=False),
        sa.ForeignKeyConstraint(
            ["master_id"],
            ["master.id"],
            name=op.f("fk_booking_slot_master_id_master"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_booking_slot")),
    )
    op.create_table(
        "reserved_slot",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("slot_id", sa.Uuid(), nullable=False),
        sa.Column("telegram_user_id", sa.BigInteger(), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.Column("is_actual", sa.Boolean(), nullable=False),
        sa.Column("is_approved", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(
            ["slot_id"],
            ["booking_slot.id"],
            name=op.f("fk_reserved_slot_slot_id_booking_slot"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["telegram_user_id"],
            ["telegram_user.id"],
            name=op.f("fk_reserved_slot_telegram_user_id_telegram_user"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_reserved_slot")),
    )


def downgrade() -> None:
    op.drop_table("reserved_slot")
    op.drop_table("booking_slot")
    op.drop_table("master")
    op.drop_table("user")
    op.drop_table("telegram_user")
