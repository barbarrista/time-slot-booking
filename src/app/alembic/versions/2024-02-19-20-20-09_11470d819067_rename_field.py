"""
Rename field

Revision ID: 11470d819067
Revises: 015d8f17df31
Create Date: 2024-02-19 20:20:09.696353

"""

from alembic import op

revision = "11470d819067"
down_revision: str | None = "015d8f17df31"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.alter_column("reserved_slot", "is_actual", new_column_name="is_deleted")


def downgrade() -> None:
    op.alter_column("reserved_slot", "is_deleted", new_column_name="is_actual")
